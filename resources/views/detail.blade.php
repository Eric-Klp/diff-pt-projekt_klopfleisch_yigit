@extends('main')

@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h1>Kennzeichen - Liste:</h1>
        </div>
        <div class="card-body">
            <div class="list-group">
                @foreach ($data as $dataObject)
                    <a href="/show/{{$dataObject->id}}" class="list-group-item list-group-item-action" aria-current="true">
                        {{$loop->index +1}}. Kennzeichenkürzel: {{$dataObject->kfz_key ?? 'Kein Datensatz vorhanden'}}, Bundesland: {{$dataObject->kfz_state ?? 'Kein Datensatz vorhanden'}}, Kreisstadt: {{$dataObject->kfz_city ?? 'Kein Datensatz vorhanden'}}, Kreis: {{$dataObject->kfz_kreis ?? 'Kein Datensatz vorhanden'}}
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection