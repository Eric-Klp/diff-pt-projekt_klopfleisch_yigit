@extends('main')

@section('content')
    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-header">
                <h1>Routenplaner</h1>
            </div>
            <form action="/route" method="POST">
                @csrf
                <div class="card-body">
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="startpunkt_input">Startpunkt</span>
                        <input type="text" class="form-control" placeholder="z.B: 'Groß-Gerau' " name="startpunkt_input" aria-label="Username" aria-describedby="startpunkt_input">
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="zielpunkt_input">Zielpunkt</span>
                        <input type="text" class="form-control" placeholder="z.B: 'Darmstadt' " name="zielpunkt_input" aria-label="Username" aria-describedby="zielpunkt_input">
                    </div>
                    <div class="d-grid gap-2">
                        <button class="btn btn-primary btn-lg" type="submit">Route berechnen</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection