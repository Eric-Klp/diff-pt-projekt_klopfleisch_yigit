@extends('main')
@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h1>Kennzeichen – Eingabe</h1>
        </div>
<form action="{{route('KfzDb.store')}}" method="post">
    @csrf
    <div class="card-body">
        <div class="input-group mb-3">
            <span class="input-group-text" id="kfz_key">Kennzeichen</span>
 <input type="text" name="kfz_key" id="kfz_key" class="form-control" placeholder="Enter Kennzeichen">
 </div>
 <div class="input-group mb-3">
    <span class="input-group-text" id="kfz_state">Bundesland</span>
 <input type="text" name="kfz_state" id="kfz_state" class="form-control" placeholder="Enter Bundesland" aria-label="Username" aria-describedby="bundesland">
 </div>
 <div class="input-group mb-3">
    <span class="input-group-text" id="kfz_kreis">Kreis</span>
 <input type="text" name="kfz_kreis" id="kfz_kreis" class="form-control" placeholder="Enter Kreis">
 </div>
 <div class="input-group mb-3">
    <span class="input-group-text" id="kfz_city">Stadt</span>
 <input type="text" name="kfz_city" id="kfz_city" class="form-control" placeholder="Enter Stadt">
 </div>
 </div>
 </div>
 <div class="d-grid gap-2">
 <button type="submit" class="btn btn-primary">Hinzufügen</button>
 </div>
</form>
</div>
</div>
@endsection