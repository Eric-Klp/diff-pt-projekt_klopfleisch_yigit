@extends('main')

@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h1>Kennzeichen Ausgabe:</h1>
        </div>
      <div class="card-body">
      <ul class="list-group">
            <li class="list-group-item"><p>Kreis: {{$kfz->kfz_kreis ?? 'Kein Datensatz vorhanden'}}</p></li>
            <li class="list-group-item"><p>Bundesland: {{$kfz->kfz_state ?? 'Kein Datensatz vorhanden'}}</p></li>
            <li class="list-group-item"><p>Stadt: {{$kfz->kfz_city ?? 'Kein Datensatz vorhanden'}}</p></li>
            <li class="list-group-item"> <p>Kennzeichen: {{$kfz->kfz_key ?? 'Kein Datensatz vorhanden'}}</p></li>
            </ul>
        </div>
        <div class="list-group">
            <a href="https://de.wikipedia.org/wiki/{{$kfz->kfz_city}}" class="list-group-item list-group-item-action list-group-item-primary"><center>Wikipedia Link</center></a>
            <a href="https://www.google.de/maps/place/{{$kfz->kfz_city}}" class="list-group-item list-group-item-action list-group-item-primary"><center>Google Maps Link</center></a>
        </div>
        <br></br>
        <div class="d-flex card-footer justify-content-between">
            <form action="/export/{{$kfz->id}}" method="POST">
                @csrf
                <input type="hidden" name="export_type" value="xml">
                <button class="btn btn-primary" type="submit">Xml Datei</button>
            </form>
            <form action="/export/{{$kfz->id}}" method="POST">
                @csrf
                <input type="hidden" name="export_type" value="csv">
                <button class="btn btn-primary" type="submit">Csv Datei</button>
            </form>
            <form action="/export/{{$kfz->id}}" method="POST">
                @csrf
                <input type="hidden" name="export_type" value="json">
                <button class="btn btn-primary" type="submit">Json Datei</button>
        </div>
    </div>
</div>
@endsection