<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KfzDbController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\RouteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [KfzDbController::class, 'index'])->name('web.kfzDb.dashboard');
Route::post('/search', [KfzDbController::class, 'search'])->name('web.kzfDb.search');
Route::get('/show/{id}', [KfzDbController::class, 'show'])->name('web.kzfDb.show');

Route::post('/export/{id}', [ExportController::class, 'getExport'])->name('export');

Route::get('/', function () {
    return view('dashboard');
}) -> name('home');

Route::get('/route', function () {
    return view('route');
}) -> name('routenplaner');

Route::get('/input', function () {
    return view('input');
}) -> name('inputkfz');


Route::post('/route', [RouteController::class, 'generateroute']);

Route::resource('KfzDb',KfzDbController::class);