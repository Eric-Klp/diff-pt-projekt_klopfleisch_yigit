<?php

namespace App\Http\Controllers;
use App\Models\KfzDb;

interface ExportInterface
{
public function startExport(): void;
public function getFilePath(): string;
}