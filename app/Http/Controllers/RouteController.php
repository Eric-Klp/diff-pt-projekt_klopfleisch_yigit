<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RouteController extends Controller
{
public function generateroute(Request $request){
        $start=request()->get('startpunkt_input');
        $ziel=request()->get('zielpunkt_input');
        $url='https://www.google.de/maps/dir/'.$start.'/'.$ziel;
        return Redirect::to($url);
    }
}
