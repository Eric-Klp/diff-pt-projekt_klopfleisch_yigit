<?php
namespace App\Http\Controllers;
use App\Http\Export\ExportCsv;
use App\Http\Export\ExportXml;
use App\Http\Export\ExportJson;
use Illuminate\Http\Request;
use App\Models\KfzDb;

class ExportController extends Controller
{
public function getExport(Request $request, $id){
        $kfzDb = KfzDb::where('id', $id)->first();
        switch (request()->get('export_type')) {
            case 'csv':
            $csvExport = new ExportCsv($kfzDb);
            $csvExport->startExport();
            sleep(1);
            $filePath = $csvExport->getFilePath();
            $fileName = $csvExport->getFileName();
            break;
            case 'json':
            $jsonExport = new ExportJson($kfzDb);
            $jsonExport->startExport();
            sleep(1);
            $filePath = $jsonExport->getFilePath();
            $fileName = $jsonExport->getFileName();
            break;
            case 'xml':
            $xmlExport = new ExportXml($kfzDb);
            $xmlExport->startExport();
            sleep(1);
            $filePath = $xmlExport->getFilePath();
            $fileName = $xmlExport->getFileName();
            break;
        }
        return response()->download($filePath, $fileName)->deleteFileAfterSend();
 }
}
