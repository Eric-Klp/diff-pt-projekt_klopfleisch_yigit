<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\KfzDb;

class KfzDbController extends Controller
{
public function index(){
        return response()->view('dashboard');
    }
public function search(Request $request){
        $data = KfzDb::where('kfz_key', request()->get('kennzeichen_input'))
        ->orWhere('kfz_kreis', request()->get('kennzeichen_input'))
        ->orWhere('kfz_state', request()->get('kennzeichen_input'))
        ->orWhere('kfz_city', request()->get('kennzeichen_input'))
        ->get();
        return response()->view('detail', ['data' => $data]);
    }
public function show(Request $request, $id){
        $kfz = KfzDb::find($id);
        return response()->view('summary', ['kfz' => $kfz]);
    }
    public function create()
    {
        return view('KfzDb.KfzDb');
    }
    public function store(Request $request)
    {
     $input = $request ->all();
     KfzDb::create($input);
     return redirect()->back();
    }

}
