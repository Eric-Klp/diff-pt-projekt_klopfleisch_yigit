<?php
namespace App\Http\Export;
use Storage;
use File;
use App\Http\Controllers\ExportInterface;
use App\Models\KfzDb;

class ExportJson implements ExportInterface
{
protected $filePath;
protected $fileName;
protected $data;
function __construct($data){
        $this->data = $data;
        $jsonExportFolderPath = storage_path('app/json');
        if(!File::exists($jsonExportFolderPath)) {
            info('json export ordner wurde erstellt');
            File::makeDirectory($jsonExportFolderPath, 0777, true, true);
        }
    }
public function startExport(): void {
        $json = [
            "Kennzeichen" => $this->data->kfz_key, 
            "Kreis" => $this->data->kfz_kreis, 
            "Kreisstadt" => $this->data->kfz_city, 
            "Bundesland" => preg_replace('/(\v|\s)+/', '', $this->data->kfz_state)
        ];
        $this->fileName = "json_export_".$this->data->kfz_key.time().".json";
        $this->filePath = storage_path('app/json/'.$this->fileName);
        $prettyJson = json_encode($json, JSON_PRETTY_PRINT);
        file_put_contents(storage_path('app/json/'.$this->fileName), stripslashes($prettyJson));
    }
public function getFilePath(): string {
        return $this->filePath;
    }
public function getFileName(): string {
        return $this->fileName;
    }
}